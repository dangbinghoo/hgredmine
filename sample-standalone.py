#!/usr/bin/env python

from wsgiref.simple_server import make_server

from hgredmine.hgweb import HgRedmine


# 'postgresql', 'mysql', 'sqlite3', and 'oracle'
DSN = {
	'ENGINE' : 'sqlite3',
	'HOST'   : '',
	'PORT'   : '',
	'NAME'	 : '/home/wnwb/workspace/deploy/dev.9thsoft.com/db/redmine.db',
	'USER'   : '',
	'PASSWORD' : '',
	'OPTIONS': {},
}

TITLE = 'Mercurial (Hg) Proxy for Redmine'
HGWEB_CFG_PATH = '/home/wnwb/workspace/deploy/redmine_auth_middleware/hgweb.config'


application = HgRedmine(TITLE, DSN, HGWEB_CFG_PATH)

httpd = make_server('', 8070, application)
print "Serving on port 8070..."

# Serve until process is killed
httpd.serve_forever()

