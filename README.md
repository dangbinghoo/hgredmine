# HgRedmine

The Mercurial (Hg) Proxy for Redmine

Version >= 2.0.0 are required for Redmine 2.x

## Prerequisites

 * Python 2.6.x or above
 * Mercurial 1.4.8 or above

## Basic Install

See [the wiki][1] for detailed installation
instructions, platform-specific notes, and HgRedmine user information.

## License 

GPL v2

[1]: https://bitbucket.org/nolith/hgredmine/wiki/Home