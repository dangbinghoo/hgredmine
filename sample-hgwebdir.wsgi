# mod_wsgi deploy

#   WSGIScriptAliasMatch ^(.*)$ /var/www/cgi-bin/hgwebdir.wsgi$1
#   <Directory "/var/www/cgi-bin/">
#     Order allow,deny
#     Allow from all
#     AllowOverride All
#     Options ExecCGI
#     AddHandler wsgi-script .wsgi
#     WSGIPassAuthorization On    #EXTREMELY IMPORTANT
#   </Directory>

# enable demandloading to reduce startup time
from mercurial import demandimport; demandimport.enable()

# Uncomment to send python tracebacks to the browser if an error occurs:
#import cgitb
#cgitb.enable()

# If you'd like to serve pages with UTF-8 instead of your default
# locale charset, you can do so by uncommenting the following lines.
# Note that this will cause your .hgrc files to be interpreted in
# UTF-8 and all your repo files to be displayed using UTF-8.
#
import os
os.environ["HGENCODING"] = "UTF-8"

from hgredmine.hgweb import HgRedmine

DSN = {
	'ENGINE' : 'mysql',
	'HOST'   : '127.0.0.1',
	'PORT'   : 3306,
	'NAME'	 : 'redmine',
	'USER'   : '',
	'PASSWORD' : '',
	'OPTIONS': {},
}

TITLE = 'Mercurial (Hg) Proxy for Redmine'
HGWEB_CFG_PATH = 'hgweb.config'

application = HgRedmine(TITLE, DSN, HGWEB_CFG_PATH)
