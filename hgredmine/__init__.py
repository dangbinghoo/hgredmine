# hgredmine - Web interface for repositories configured in Redmine projects.
#
# Copyright (c) 2009-2010 Brant Young <brant@9thsoft.com>
# Copyright (c) 2010-2011 Alessio Caiazza <nolith@abisso.org>
#
# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.

__all__ = ["hgweb"]